/**----------------------------------------------------------------------------------------------------------------*
 *                                                                                                                 *
 *     NAME: FIBONACCI                          AUTHOR: MANSUR MANSUR             DATE:  MARCH 17th, 2020          *
 *     ---------------                         ----------------------             -----------------------          *
 *                                                                                                                 *
 *                                                                                                                 *
 *    PURPOSE: This program uses the dynamic programming algorithm to get nth value in fibonacci series.           *
 *                                                                                                                 *
 *                                                                                                                 *
 *                                                                                                                 *
 *    1. WHAT IS DYNAMIC PROGRAMMING ALGORITHM ?                                                                   *
 *                                                                                                                 *
 *      It is used to optimize recursive algorithms, as they tend to scale exponentially. The main idea is to      *
 *      break down complex problems (with many recursive calls) into smaller sub-problems then save them into      *
 *      memory so that we don't have to recalculate them each time we use them.                                    *
 *                                                                                                                 *
 *   2. WHEN SOLVING A PROBLEM USING DYNAMIC PROGRAMMING, WE HAVE TO FOLLOW THREE STEPS:                           *
 *                                                                                                                 *
 *      a) determine the recurrence relation that applies to said problem                                          *
 *      b) Initialize the memory/array/matrix's starting values                                                    *
 *      c) Make sure that when we make a "recursive call" (access the memoized solution of a sub-problem) it's     *
 *                 always solved in advance.                                                                       *
 *                                                                                                                 *
 *                                                                                                                 *
 *    one disadvantage of dynamic programming is it takes unnecessary memory but faster than recursive.            *
 *                                                                                                                 *
 *                                                                                                                 *
 *         reference: stackabuse.com                                                                               *
 *                                                                                                                 *
 *                                                                                                                 *
 *----------------------------------------------------------------------------------------------------------------*/
import java.util.*;

//main class
public class Main {

    //main method
    public static void main(String[] args)
    {
        //create a scanner object
        Scanner scanner = new Scanner(System.in);


        //user input
        System.out.print("Enter the nth number in fibonacci series: ");
        int n = scanner.nextInt();    //read user input

        //invoke the fibo function
        System.out.print("The "+n+" in Fibonacci series using Recursive method is:  "+fibRecursive(n)+"\n");





        //invoke dynamic programming
        System.out.print("The "+n+" in Fibonacci series using Dynamic programming method is:  "+dynamicProgramming(n)+" \n");
    }


    /**----------------------------------------------------------------------------------------------------------------*
     *                                                                                                                 *
     *     Method name: fiboRecursive()
     *     Description: it returns nth value in a fibonacci series using recursive algorithm
     *     Algorithm:
     *            1. check if num is < or = 1
     *            2. if so then return the num
     *            3. else call the same method passing num-1 and also call same method pass num-2
     *            4. return num-1 + num-2
     *     return type: int
     *     parameters: num (integer)
     *     local variables: void
     *----------------------------------------------------------------------------------------------------------------*/
    public static int fibRecursive(int num)
    {
        //checks num if it is less than or equal to 1
        if(num <= 1)
            return num;
        else
            return fibRecursive(num-1)+fibRecursive(num-2);
    }



    /**----------------------------------------------------------------------------------------------------------------*
     *                                                                                                                 *
     *     Method name:  dynamicProgramming()
     *     Description:  it returns nth value in a fibonacci series using recursive algorithm
     *     Algorithm:
     *            1. declare array
     *            2. assign the array index 0 and 1
     *            3. using loop constantly get f[i] = f[i-1] + f[i-2]
     *            4. return when the nth value is reached in the series
     *     return type: int
     *     parameters: num (integer)
     *     local variables: integer array (f)
     *----------------------------------------------------------------------------------------------------------------*/
    //dynamic programming function
    public static int dynamicProgramming(int num)
    {
        //declare array
        int []f = new int[20];
        f[0] = 0;
        f[1] = 1;

        for (int i = 2; i <= num; i++ )
        {
            f[i] = f[i-1] + f[i-2];
        }

        //return
        return f[num];
    }
}
